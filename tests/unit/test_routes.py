import json
import os
import unittest
from unittest import mock

import numpy as np
from aiohttp import web
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from tangogql.routes import (
    NumpyArrayEncoder,
    db_handler,
    graphiql,
    graphiql_noslash,
    socket_handler,
)

# Mocking PyTango and related modules
mock_pytango = mock.Mock()
mock_pytango.Database.return_value = mock.Mock()
modules_to_mock = {
    "PyTango": mock_pytango,
    "tango": mock_pytango,
}


patcher = mock.patch.dict("sys.modules", modules_to_mock)
patcher.start()


class TestRoutes(AioHTTPTestCase):

    async def get_application(self):
        app = web.Application()
        app.router.add_get("/graphiql", graphiql_noslash)
        app.router.add_get("/graphiql/", graphiql)
        app.router.add_post("/db", db_handler)
        app.router.add_get("/socket", socket_handler)
        return app

    def mockenv(self, **envvars):
        return mock.patch.dict(os.environ, envvars)

    @unittest_run_loop
    async def test_graphiql_no_slash(self):
        resp = await self.client.get("/graphiql")
        assert resp.status == 200
        assert "GraphiQL" in await resp.text()

    @unittest_run_loop
    async def test_graphiql(self):
        resp = await self.client.get("/graphiql/")
        assert resp.status == 200
        assert "GraphiQL" in await resp.text()

    def test_numpy_array_encoder(self):
        encoder = NumpyArrayEncoder()
        d = {"key": "value", "key2": 123}
        assert (
            json.dumps(d, cls=NumpyArrayEncoder)
            == '{"key": "value", "key2": 123}'
        )
        assert encoder.default(
            np.array([["a", "b", "c"], ["d", "e", "f"]])
        ) == [
            ["a", "b", "c"],
            ["d", "e", "f"],
        ]
        assert encoder.default(np.array([[1, 2, 3], [4, 5, 6]], np.int32)) == [
            [1, 2, 3],
            [4, 5, 6],
        ]


if __name__ == "__main__":
    unittest.main()
