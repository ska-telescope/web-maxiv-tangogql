Query Module Documentation
==========================

Module containing Queries
-------------------------

This module defines GraphQL queries and mutations for interacting with the Tango database.

Examples on Query and Mutation
==============================

Fetch Information of Devices
----------------------------

.. code:: python

    query{
        devices{ 
            name                # e.g. get the names of all devices
        }
    }  

    query{
        devices(pattern: "*tg_test*"){            # filter result with pattern
            name
        }
    }

Accessing Attributes
--------------------

.. code:: python 

    query{
        devices(pattern: "sys/tg_test/1"){
            name,
            attributes {
                name,
                datatype,
                }
            }
        }

    query{
        devices(pattern: "sys/tg_test/1"){
            name,
            attributes(pattern: "*scalar*") {
                    name,
                    datatype,
                    dataformat,
                    label,
                    unit,
                    description,
                    value,
                    quality,
                    timestamp
                }
                server{
                id,
                host
            }
        }
    }

Query All Domains
-----------------

.. code:: python 

    query{
        domains(pattern: "*"){
            name
        }
    }

Query All Families in a Domain
------------------------------

.. code:: python 

    query{
        families(domain: "domain_name", pattern: "*"){
            name
        }
    }

Query All Members in a Family
-----------------------------

.. code:: python 

    query{
        members(domain: "domain_name", family: "family_name", pattern: "*"){
            name
        }
    }

Query All Servers
-----------------

.. code:: python 

    query{
        servers(pattern: "*"){
            name
        }
    }

Query All Server Instances
--------------------------

.. code:: python 

    query{
        instances(server: "server_name", pattern: "*"){
            name
        }
    }

Query All Classes
-----------------

.. code:: python 

    query{
        classes(pattern: "*"){
            name
        }
    }

Query All Classes and Corresponding Devices
-------------------------------------------

.. code:: python 

    query{
        classes(pattern: "*"){
            name
            devices {
                name
            }
        }
    }

Fetch Device by Name
--------------------

.. code:: python 

    query{
        device(name: "device_name"){
            name
        }
    }

Fetch Attributes by Full Names
------------------------------

.. code:: python 

    query{
        attributes(full_names: ["device_name/attribute_name"]){
            name
        }
    }

Fetch Commands by Full Names
----------------------------

.. code:: python 

    query{
        commands(full_names: ["device_name/command_name"]){
            name
        }
    }

Fetch Metrics
-------------

.. code:: python 

    query{
        metrics{
            execution_time {
                query {
                    argument
                    duration
                }
                mutation {
                    argument
                    duration
                }
            }
            subscribed_attrs {
                name
                attribute
                listeners
                use_evt
                event_type
                accessible
            }
        }
    }

Class Descriptions
==================

Query
-----

This class contains all the queries.

Attributes:
    version (String): Get the version of the schema.
    info (String): Get general information about the schema.
    devices (List[Device]): Get a list of devices.
    device (Field[Device]): Get a device by name.
    domains (List[Domain]): Get a list of domains.
    families (List[Family]): Get a list of families by domain and pattern.
    members (List[Member]): Get a list of members by domain, family, and pattern.
    servers (List[Server]): Get a list of servers.
    instances (List[ServerInstance]): Get a list of server instances.
    classes (List[DeviceClass]): Get a list of classes.
    attributes (List[DeviceAttribute]): Get a list of attributes by full names.
    commands (List[DeviceCommand]): Get a list of commands by full names.
    metrics (Field[Metrics]): Get metrics data.

Domain
------

This class represents a domain.

Attributes:
    name (String): The name of the domain.
    families (List[Family]): A list of families in the domain.

Methods:
    resolve_families: Fetch a list of families using a pattern.

Family
------

This class represents a family.

Attributes:
    name (String): The name of the family.
    domain (String): The domain of the family.
    members (List[Member]): A list of members in the family.

Methods:
    resolve_members: Fetch a list of members using a pattern.

Member
------

This class represents a member.

Attributes:
    domain (String): The domain of the member.
    family (String): The family of the member.
    info (property): Fetch the member's device info.

DeviceClass
-----------

This class represents a device class.

Attributes:
    name (String): The name of the class.
    server (String): The server of the class.
    instance (String): The instance of the class.
    devices (List[Device]): A list of devices in the class.

Server
------

This class represents a server.

Attributes:
    name (String): The name of the server.
    instances (List[ServerInstance]): A list of instances of the server.

Methods:
    resolve_instances: Fetch a list of server instances using a pattern.

ServerInstance
--------------

This class represents a server instance.

Attributes:
    name (String): The name of the server instance.
    server (String): The server of the instance.
    classes (List[DeviceClass]): A list of device classes in the instance.

Methods:
    resolve_classes: Fetch a list of device classes using a pattern.

Record
------

This class holds execution details of a single query.

Attributes:
    argument (String): The arguments of the query.
    duration (Float): The execution duration of the query.

ExecutionTime
-------------

This class contains query and mutation details.

Attributes:
    mutation (List[Record]): A list of mutation records.
    query (List[Record]): A list of query records.

SubscribedAttrs
---------------

This class represents subscribed attributes.

Attributes:
    name (String): The name of the subscribed attribute.
    attribute (String): The attribute name.
    listeners (Int): The number of listeners.
    use_evt (String): The use event.
    event_type (String): The event type.
    accessible (Boolean): Whether the attribute is accessible.

Metrics
-------

This class represents metrics data.

Attributes:
    execution_time (Field[ExecutionTime]): The execution time details.
    subscribed_attrs (List[SubscribedAttrs]): A list of subscribed attributes.
