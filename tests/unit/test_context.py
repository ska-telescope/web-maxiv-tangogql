import jwt
import pytest
import requests
import requests_mock

from tangogql.config import Config
from tangogql.context import ClientInfo, build_context


class TestClientInfo:
    def test_init(self):
        with pytest.raises(TypeError, match="user must be a string or None"):
            ClientInfo(user=123, groups=[])

        with pytest.raises(TypeError, match="groups must be a list"):
            ClientInfo(user=None, groups=123)

        c = ClientInfo(user=None, groups=[])
        assert c.user is None
        assert c.groups == []

        c = ClientInfo(user="test", groups=["group1"])
        assert c.user == "test"
        assert c.groups == ["group1"]


def test_build_context(tmpdir):
    d = tmpdir.mkdir("subdir")
    fh = d.join("config.json")
    fh.write(
        '{"secret":"secret","allowable_commands":[],"required_groups":["group1", "group2"]}'
    )
    config = Config(fh)

    # Correcting the encode method to avoid calling decode
    encoded_jwt = jwt.encode(
        {"username": "user", "groups": ["group1", "group2"]},
        "secret",
        algorithm="HS256",
    )
    cookies = {"taranta_jwt": str(encoded_jwt)}  # Updated cookie name

    with requests_mock.Mocker() as m:
        m.get("http://test.com", cookies=cookies)
        context = build_context(requests.get("http://test.com"), config)

        assert "client" in context.keys()
        assert "config" in context.keys()
        assert context["config"] == config
        assert context["client"].user == "user"
        assert context["client"].groups == ["group1", "group2"]
