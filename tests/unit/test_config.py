import os

import pytest

from tangogql.config import Config, ConfigError


@pytest.fixture
def config_file(tmpdir):
    d = tmpdir.mkdir("subdir")
    fh = d.join("config.json")
    return fh


def test_errors_handling(config_file):
    no_secret = '{"allowable_commands":[],"required_groups":[] }'
    secret_not_str = (
        '{"secret":123,"allowable_commands":[],"required_groups":[]}'
    )
    groups_not_str = (
        '{"secret":"","allowable_commands":[],"required_groups":[123,456]}'
    )

    config_file.write(no_secret)
    if "TANGOGQL_SECRET" not in os.environ.keys():
        with pytest.raises(
            ConfigError, match="Config error: no secret provided"
        ):
            Config(config_file)

    config_file.write(secret_not_str)
    with pytest.raises(
        ConfigError, match="Config error: secret must be a string"
    ):
        Config(config_file)

    config_file.write(groups_not_str)
    with pytest.raises(
        ConfigError,
        match="Config error: required_groups must consist of strings",
    ):
        Config(config_file)


def test_parsing(config_file):
    proper_config = '{"secret":"test", "allowable_commands":[],"required_groups":["group1", "group2"]}'
    config_file.write(proper_config)
    c = Config(config_file)
    assert c.secret
    assert c.required_groups


def test_error_class():
    ce = ConfigError("test")
    assert str(ce) == "Config error: test"
